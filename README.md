* APLICATIE - FridgeToPlate
* Aplicatie care pe baza unor ingrediente introduse de catre un utilizator inregistrat, sistemul va sugera o lista de retete.

* PROBLEMA PE CARE O REZOLVA: Lipsa ideilor de retete prin sugerarea automata a acestora.

* CERINTELE - CE VREAU SA FACA APLICATIA : pe baza unei liste de ingrediente introdusa de catre un utilizator cu cont,sa se afiseze liste de retete compatibile cu ingredientele date de catre acesta.

* TIPURI DE UTILIZATORI : 
	* Administrator(se ocupa de baza de date)
	* Persoana/User(persoana care are un cont, introduce ingrediente si primeste o lista de retete) 
	* (*) Client(poate doar sa isi faca un cont nou)

* Ca dezvoltare ulterioara :
	* implementarea unui sistem de Gamification prin adaugarea unui atribut "Scor" la fiecare utilizator inregistrat.
	* Scorul va reprezenta un numar de puncte acumulate in functie de retetele gatite de catre un utilizator (fiecare reteta va avea un anumit punctaj in functie de dificultatea acesteia).		
	
* Detalii de implementare :

	* Am folosit o arhitectura bazate pe pachete pentru implementarea aplicatiei, precum : Model, Controller, Repository si Factory.
	* Pachetul Model contine clasele corespunzatoare tabelelor din baza de date : Person si Ingredient.
	* Pachetul Controler contine o serie de metode implementate pentru a comunica cu baza de date : endpoint-urile.
	* Pachetul Repository contine doua interfete PersonRepository si IngredientRepository care extind fiecare clasa CrudRepository pt comunicarea cu baza de date.
	
	* Conexiunea la baza de date a fost facuta cu ajutorul SpringBoot si testata cu Postamn
	* Am folosit FactoryPattern pentru Ingrediente, avand 3 tipuri de ingrediente : Dairy, Vegetables si Meats.
	* Endpoint-uri scrise pentru Ingrediente : deteleById, findById, addIngredient, getAllIngredients.